package main

func main() {
	colors := map[string]string{
		"red":   "#ff0000",
		"green": "#4bf745",
	}
	for key, value := range colors {
		println(key, value)
	}
}
