package main

// go run 23_Multiple_Return_Values.go deck.go
func main() {
	cards := newDeck()
	hand, remainingCards := deal(cards, 5)
	hand.print()
	remainingCards.print()
}
