package main

import "fmt"

type Contact struct {
	email   string
	zipCode int
}
type Person struct {
	FirstName string
	LastName  string
	Contact   Contact
}

func (p *Person) updateName(newFirstName string) {
	(*p).FirstName = newFirstName
}

func (p Person) print() {
	fmt.Printf("%+v", p)
}

func main() {
	person := Person{FirstName: "Alex", LastName: "Anderson"}
	fmt.Println(person)
	fmt.Println(person.FirstName)
	fmt.Println(person.LastName)

	person2 := Person{}
	person2.FirstName = "Bob"
	person2.LastName = "Barker"
	fmt.Println(person2)
	fmt.Printf("%+v", person2)
	fmt.Println("\n")

	var person3 Person
	person3.FirstName = "Charlie"
	person3.LastName = "Chaplin"
	fmt.Println(person3)
	fmt.Printf("%+v", person3)
	fmt.Println("\n")

	person4 := Person{
		FirstName: "Allen",
		LastName:  "Tan",
		Contact: Contact{
			email:   "allen@tan.com",
			zipCode: 12345,
		},
	}
	fmt.Println(person4)
	fmt.Printf("%+v", person4)
	fmt.Println("\n")

	person4.updateName("Allen2")
	person4.print()
	fmt.Println("\n")

	person4Pointer := &person4
	person4Pointer.updateName("Allen3")
	person4.print()
	fmt.Println("\n")
}
