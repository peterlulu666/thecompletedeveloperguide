package main

import "fmt"

func main() {
	cards := newDeck()
	if err := cards.saveToFile("my_cards"); err != nil {
		fmt.Println(err)
	}
}
