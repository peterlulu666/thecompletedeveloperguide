package main

import "fmt"

func newCard() string {
	return "Five of Diamonds"
}

// go run 19_Custom_Type_Declarations.go deck.go
func main() {
	cards := deck{"Ace of Diamonds", newCard()}
	cards = append(cards, "Six of Spades")
	for i, card := range cards {
		fmt.Println(i, card)
	}
	cards.print()
}
